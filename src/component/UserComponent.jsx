import React from "react";
import { Button, Container } from "react-bootstrap";
import { Link, Outlet } from "react-router-dom";

export default function UserComponent() {
  return (
    <div>
      <h1>User Page</h1>
      <Button variant="success" as={Link} to="profile">
        User Profile
      </Button>
      <Button variant="success" as={Link} to="detail">
        User Detail
      </Button>
      <Container>
        <Outlet />
      </Container>
    </div>
  );
}
