import React from 'react'
import { useParams } from 'react-router-dom'

export default function PostComponent() {
    const {param} = useParams();
  return (
    <div>
        <h1>{param}</h1>
    </div>
  )
}
