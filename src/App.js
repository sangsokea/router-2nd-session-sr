import logo from "./logo.svg";
import "./App.css";
import { Route, Routes, useNavigate } from "react-router-dom";
import HomeComponent from "./component/HomeComponent";
import UserComponent from "./component/UserComponent";
import NavbarComponent from "./component/NavbarComponent";
import UserDetail from "./component/UserDetail";
import UserProfile from "./component/UserProfile";
import PostComponent from "./component/PostComponent";
import LoginComponent from "./component/LoginComponent";
import { useEffect, useState } from "react";
import ProtectedRoute from "./component/ProtectedRoute";

function App() {
  const [isAuth, setIsAuth] = useState(false);
  const navigate = useNavigate()
  const setAuth = () => {
    setIsAuth(!isAuth);
  };
  useEffect(
    () => {
      console.log(isAuth);
      isAuth ? navigate('/user') : navigate('/')
    }, [isAuth]
  )
  
  return (
    <div className="App">
      <NavbarComponent setAuth = {setAuth}/>
      <Routes>
        <Route path="/" element={<HomeComponent />} />
        <Route path="/user" element={<ProtectedRoute auth={isAuth}>
          <UserComponent/>
        </ProtectedRoute>}/>
        <Route path="post/:param" element={<PostComponent />} />
        <Route path="login" element={<LoginComponent setAuth={setAuth} />} />
      </Routes>
    </div>
  );
}

export default App;
